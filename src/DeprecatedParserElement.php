<?php

namespace fafcms\parser;

use Faf\TemplateEngine\Helpers\ParserElement;
use Closure;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class DeprecatedParserElement
 * @package fafcms\parser
 */
class DeprecatedParserElement extends ParserElement
{
    public $deprecatedName;
    public $deprecatedReplacement;

    /**
     * {@inheritdoc}
     */
    public function init(): void
    {
        parent::init();

        if ($this->deprecatedReplacement instanceof Closure) {
            $this->deprecatedReplacement = call_user_func($this->deprecatedReplacement, $this->parser);
        }

        if (isset($this->deprecatedReplacement['init']) && $this->deprecatedReplacement['init'] instanceof Closure) {
            call_user_func($this->deprecatedReplacement['init'], $this);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return $this->deprecatedName;
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-parser', 'Handles deprecated parser elements');
    }

    /**
     * {@inheritdoc}
     */
    public function allowedTypes(): ?array
    {
        return $this->deprecatedReplacement['allowedTypes']??null;
    }

    /**
     * {@inheritdoc}
     */
    public function allowedParents(): ?array
    {
        return $this->deprecatedReplacement['allowedParents']??null;
    }

    /**
     * {@inheritdoc}
     */
    public function bootstrap(): void
    {
        if (isset($this->deprecatedReplacement['bootstrap']) && $this->deprecatedReplacement['bootstrap'] instanceof Closure) {
            call_user_func($this->deprecatedReplacement['bootstrap'], $this);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return call_user_func(
            $this->deprecatedReplacement['replacement'],
            $this->parser->getType(),
            str_replace($this->parser->name.'-', '', $this->getParser()->getCurrentTagName()),
            $this->domNode,
            new Crawler($this->domNode),
            $this->parser->data,
            $this->parser->getLanguage(),
            $this->parser->getReturnRawData(),
            $this->parser->getNodeStats()
        );
    }
}
