<?php

namespace fafcms\parser;

use yii\base\BaseObject;

/**
 * Class ElementSetting
 *
 * @package fafcms\parser
 */
class ElementSetting extends BaseObject
{
    public const ATTRIBUTE_NONE = 0;
    public const ATTRIBUTE_STRING = 1;

    /**
     * @var string
     */
    public $name;

    /**
     * @var array
     */
    public $aliases = [];

    /**
     * @var string
     */
    public $label;

    /**
     * @var int
     */
    public $attribute = self::ATTRIBUTE_NONE;

    /**
     * @var string
     */
    public $element;

    /**
     * @var bool
     */
    public $content = false;

    /**
     * @var bool
     */
    public $multiple = false;

    /**
     * @var bool
     */
    public $rawData = false;

    /**
     * @var bool
     */
    public $multipleAttributeExpression = '/^{{name}}(-(.*))?$/i';

    /**
     * @var bool
     */
    public $attributeNameAsKey = false;

    /**
     * @var array
     */
    public $rules = [];
}
