<?php

namespace fafcms\parser\component;

use fafcms\fafcms\helpers\StringHelper;
use fafcms\helpers\ClassFinder;
use fafcms\parser\DeprecatedParserElement;
use fafcms\parser\interfaces\ActionInterface;
use Faf\TemplateEngine\Helpers\ParserElement;
use Faf\TemplateEngine\Parser as FafteParser;
use Symfony\Component\DomCrawler\Crawler;
use Yii;
use yii\base\Component;
use yii\base\InvalidArgumentException;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\log\Logger;
use yii\validators\DefaultValueValidator;
use yii\validators\Validator;
use Yii2Extended\Yii2Log\Psr3ToYii2Logger;
use Yii2Extended\Yii2SimpleCache\Psr16ToYii2SimpleCache;

class Parser extends Component
{
    public const ROOT = 'root';

    public const TYPE_PAGE = 0;
    public const TYPE_TEXT = 1;
    public const TYPE_PDF = 2;
    public const TYPE_TEXT_MAIL = 3;
    public const TYPE_HTML_MAIL = 4;

    /**
     * @var array
     */
    public array $data = [];

    public $name = 'fafcms';

    /**
     * @var string
     */
    private $_parentTagName = self::ROOT;

    public function checkConditionArray($type, $conditions)
    {
        $result = false;

        foreach ($conditions as $condition) {
            if ($condition) {
                $result = true;

                if ($type === 'or') {
                    break;
                }
            } elseif ($type === 'and') {
                $result = false;
                break;
            }
        }

        return $result;
    }

    public function fullTrim($string)
    {
        return trim(str_replace('&nbsp;', mb_chr(0xA0, 'UTF-8'), $string), " \t\n\r\0\x0B".mb_chr(0xC2, 'UTF-8').mb_chr(0xA0, 'UTF-8'));
    }

    public FafteParser $fafte;

    public function getParams($type, $parentTagName, $node, $crawler, $data, $language)
    {
        $params = [];

        $params[0] = $node->getAttribute('param-1');

        if (empty($params[0])) {
            $param = $crawler->filterXPath($this->name . '-' . $parentTagName . '/' . $this->name . '-param-1');
            $param = (count($param) > 0?$param->html():'');

            $params[0] = $this->parse($type, $param, $parentTagName, $data, $language, true);

            if (!is_string($params[0])) {
                $params[0] = $params[0][0] ?? null;
            }
        }

        $params[1] = $node->getAttribute('param-2');

        if (empty($params[1])) {
            $param = $crawler->filterXPath($this->name . '-' . $parentTagName . '/' . $this->name . '-param-2');
            $param = (count($param) > 0?$param->html():'');

            $params[1] = $this->parse($type, $param, $parentTagName, $data, $language, true);

            if (!is_string($params[1])) {
                $params[1] = $params[1][0] ?? null;
            }
        }

        return $params;
    }

    public $type;
    public $language = null;

    /**
     * @var array|null
     */
    protected ?array $parserElementClasses = null;

    public function init()
    {
        $this->fafte = new FafteParser([
            //'logger' => $logger,
           // 'logger' => new Psr3ToYii2Logger(),
            'cache' => new Psr16ToYii2SimpleCache(Yii::$app->cache),
            'name' => 'fafcms',
           // 'mode' => FafteParser::MODE_DEV,
            'language' => $this->language ?? Yii::$app->language,
            'settings' => [
                'default-input-time-zone' => Yii::$app->getFormatter()->defaultTimeZone,
                'default-display-time-zone' => Yii::$app->getFormatter()->timeZone,
            ]
        ]);

        parent::init();
    }

    /**
     * @param $type
     * @param $string
     * @param null $currentTagName
     * @param null $data
     * @param null $language
     * @param bool $getRawData
     * @return array|mixed|string|string[]|null
     */
    public function parse($type, $string, $currentTagName = null, $data = null, $language = null, $getRawData = false)
    {

        if ($language !== null) {
            $this->fafte->setLanguage($language);
        }

        if ($this->parserElementClasses === null) {
            $finder = new ClassFinder();

            $filter = [
                'extends' => ParserElement::class,
            ];

            $parserElementClasses = [
                $finder->findClasses(Yii::getAlias('@project/elements'), $filter),
                $finder->findClasses(Yii::getAlias('@vendor/finally-a-fast/fafcms-module-parser/src/deprecated'), [
                    'extends' => DeprecatedParserElement::class,
                ])
            ];

            foreach (Yii::$app->fafcms->getLoadedPluginPaths() as $loadedPluginPath) {
                $parserElementClasses[] = $finder->findClasses($loadedPluginPath . '/elements', $filter);
            }

            $this->parserElementClasses = array_merge([], ...$parserElementClasses);

            $this->fafte->addElements(array_column($this->parserElementClasses, 'fullyQualifiedClassName'));
        }

        $data = array_merge($this->data, $data ?? []);

        return $this->fafte->setType($type)->setData($data)->setReturnRawData($getRawData)->parse($string);
    }

}
