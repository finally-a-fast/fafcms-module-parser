<?php

namespace fafcms\parser\deprecated;

use fafcms\parser\DeprecatedParserElement;
use Closure;
use Symfony\Component\DomCrawler\Crawler;
use Yii;

/**
 * Class Percent
 *
 * @package fafcms\parser\deprecated
 */
class Percent extends DeprecatedParserElement
{
    public $deprecatedName = 'percent';
    public $deprecatedReplacement;

    /**
     * {@inheritdoc}
     */
    public function init(): void
    {
        $this->deprecatedReplacement = [
            'replacement' => function($type, $parentTagName, $node, $crawler, $data, $language, $getRawData) {
                $decimals = $node->hasAttribute('decimals')?$node->getAttribute('decimals'):null;
                $minFractionDigits = $node->hasAttribute('min_fraction_digits')?$node->getAttribute('min_fraction_digits'):0;
                $maxFractionDigits = $node->hasAttribute('max_fraction_digits')?$node->getAttribute('max_fraction_digits'):2;
                $useInnerHTML = true;
                if ($decimals === null) {
                    $decimalsChildren = $crawler->filterXPath($this->parser->name.'-'.$parentTagName.'/'.$this->parser->name.'-'.$parentTagName.'-decimals');
                    if (count($decimalsChildren) > 0) {
                        $useInnerHTML = false;
                        $decimals = $this->parser->fullTrim($this->parser->parseElements($decimalsChildren->html(), $this->parser->getName() . '-' . $parentTagName));
                    }
                }
                $value = $node->hasAttribute('value')?$node->getAttribute('value'):null;
                if ($value === null) {
                    $valueChildren = $crawler->filterXPath($this->parser->name.'-'.$parentTagName.'/'.$this->parser->name.'-'.$parentTagName.'-value');
                    if (count($valueChildren) > 0) {
                        $useInnerHTML = false;
                        $value = $valueChildren->html();
                    }
                }
                if ($useInnerHTML) {
                    $value = $crawler->html();
                }
                if ($value !== null) {
                    $value = $this->parser->fullTrim($this->parser->parseElements($value, $this->parser->getName() . '-' . $parentTagName));
                }
                try {
                    $parsedPercent = Yii::$app->formatter->asPercent($value / 100, $decimals, [
                        NumberFormatter::MIN_FRACTION_DIGITS => $minFractionDigits,
                        NumberFormatter::MAX_FRACTION_DIGITS => $maxFractionDigits
                    ]);
                } catch (\Exception $e) {
                    Yii::$app->log->logger->log('Cannot format Percent: '.$value.'. '.$e->getMessage(), Logger::LEVEL_ERROR);
                }
                return $parsedPercent??'';
            },
        ];

        parent::init();
    }
}
