<?php

namespace fafcms\parser\deprecated;

use fafcms\parser\DeprecatedParserElement;
use Symfony\Component\DomCrawler\Crawler;
use fafcms\filemanager\Bootstrap;
use fafcms\filemanager\models\File;
use fafcms\filemanager\models\Filetype;
use yii\helpers\Html;
use fafcms\filemanager\assets\VideoJsYoutubeAsset;
use Yii;

/**
 * Class EmbedImage
 *
 * @package fafcms\parser\deprecated
 */
class EmbedImage extends DeprecatedParserElement
{
    public $deprecatedName = 'embed-image';
    public $deprecatedReplacement;

    /**
     * {@inheritdoc}
     */
    public function init(): void
    {
        $this->deprecatedReplacement = [
            'replacement' => function ($type, $parentTagName, $node, $crawler, $data, $language) {
                    $id = $node->hasAttribute('id') ? $node->getAttribute('id') : null;
                    $imagePath = File::getFilePath(File::find()->where(['id' => $id])->one());
                    return Html::img($imagePath, [
                        'style' => [
                            'width' => $node->hasAttribute('width') ? $node->getAttribute('width') : null,
                            'height' => $node->hasAttribute('height') ? $node->getAttribute('height') : null,
                        ]
                    ]);
                },
            'allowedTypes' => [
                2,
            ],
        ];

        parent::init();
    }
}
