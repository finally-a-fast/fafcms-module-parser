<?php

namespace fafcms\parser\deprecated;

use fafcms\parser\DeprecatedParserElement;
use Symfony\Component\DomCrawler\Crawler;
use Yii;
use fafcms\filemanager\Bootstrap;
use fafcms\filemanager\models\File;
use fafcms\filemanager\models\Filetype;
use yii\helpers\Html;
use fafcms\filemanager\assets\VideoJsYoutubeAsset;

/**
 * Class Media
 *
 * @package fafcms\parser\deprecated
 */
class Media extends DeprecatedParserElement
{
    public $deprecatedName = 'media';
    public $deprecatedReplacement;

    /**
     * {@inheritdoc}
     */
    public function init(): void
    {
        $this->deprecatedReplacement = [
            'replacement' => function($type, $parentTagName, $node, $crawler, $data, $language) {
                    $src = $node->hasAttribute('src')?$node->getAttribute('src'):null;
                    $alt = $node->hasAttribute('alt')?$node->getAttribute('alt'):null;
                    $class = $node->hasAttribute('class')?$node->getAttribute('class'):null;
                    $mediatype = $node->hasAttribute('type')?$node->getAttribute('type'):null;
                    $responsive = $node->hasAttribute('responsive')?$node->getAttribute('responsive'):null;
                    $convertSvg = $node->hasAttribute('convert-svg')?$node->getAttribute('convert-svg'):'false';
                    if ($responsive === null) {
                        $responsiveChildren = $crawler->filterXPath($this->parser->name.'-'.$parentTagName.'/'.$this->parser->name.'-'.$parentTagName.'-responsive');
                        if (count($responsiveChildren) > 0) {
                            $responsive = $this->parser->fullTrim($this->parser->parseElements($responsiveChildren->html(), $this->parser->getName() . '-' . $parentTagName));
                        }
                    }
                    $responsive = $responsive === 'true';
                    if ($src === null) {
                        $srcChildren = $crawler->filterXPath($this->parser->name.'-'.$parentTagName.'/'.$this->parser->name.'-'.$parentTagName.'-src');
                        if (count($srcChildren) > 0) {
                            $src = $this->parser->fullTrim($this->parser->parseElements($srcChildren->html(), $this->parser->getName() . '-' . $parentTagName));
                        }
                    }
                    if ($alt === null) {
                        $altChildren = $crawler->filterXPath($this->parser->name.'-'.$parentTagName.'/'.$this->parser->name.'-'.$parentTagName.'-alt');
                        if (count($altChildren) > 0) {
                            $alt = $this->parser->fullTrim($this->parser->parseElements($altChildren->html(), $this->parser->getName() . '-' . $parentTagName));
                        }
                    }
                    if ($class === null) {
                        $classChildren = $crawler->filterXPath($this->parser->name.'-'.$parentTagName.'/'.$this->parser->name.'-'.$parentTagName.'-class');
                        if (count($classChildren) > 0) {
                            $class = $this->parser->fullTrim($this->parser->parseElements($classChildren->html(), $this->parser->getName() . '-' . $parentTagName));
                        }
                    }
                    $options = [];
                    $wrapperOptions = [
                        'class' => $class
                    ];
                    if ($alt !== null) {
                        $options['alt'] = $alt;
                    }
                    if ($src !== null) {
                        if (is_numeric($src)) {
                            $file = File::find()->select([
                                    File::tableName().'.id',
                                    File::tableName().'.filegroup_id',
                                    File::tableName().'.filetype_id',
                                    File::tableName().'.filename',
                                    File::tableName().'.alt',
                                    Filetype::tableName().'.mediatype',
                                    Filetype::tableName().'.mime_type',
                                    Filetype::tableName().'.default_extension'
                                ])
                                ->innerJoinWith('filetype', false)
                                ->where([
                                    File::tableName().'.id' => $src
                                ])
                                ->asArray()
                                ->one();
                            if ($file !== null) {
                                $name = $node->hasAttribute('name')?$node->getAttribute('name'):null;
                                $sizes = [];
                                foreach ($node->attributes as $attr) {
                                    if (mb_stripos($attr->nodeName, 'size') === 0) {
                                        $sizes[] = explode(',', $attr->nodeValue);
                                    }
                                }
                                if ($file['mediatype'] === 'image' || $file['mime_type'] === 'application/pdf' || ($file['mediatype'] === 'video' && ($mediatype === 'animation' || $mediatype === 'thumbnail'))) {
                                    $defaultOptions = '';
                                    if ($file['mediatype'] === 'video') {
                                        $defaultOptions .= 'type:' . $mediatype;
                                    }
                                    if ($file['mime_type'] === 'image/svg+xml' && $convertSvg !== 'true') {
                                        Html::addCssClass($wrapperOptions, 'fafcms-filemanager-image-wrapper');
                                        return Html::tag('div', Html::img(File::getUrl($file), $options), $wrapperOptions);
                                    }
                                    $loader = $node->hasAttribute('loader')?$node->getAttribute('loader') : null;
                                    if ($loader === 'false' || $loader === false) {
                                        $loader = false;
                                    } elseif ($loader !== null && $loader !== '') {
                                        $loader = explode(',', $loader);
                                    }
                                    return File::getPicture($file, $name, $sizes, $responsive, $loader, $options, $wrapperOptions, $defaultOptions);
                                }
                                if ($file['mediatype'] === 'video') {
                                    $poster = $node->hasAttribute('poster') ? $node->getAttribute('poster') : null;
                                    foreach ($node->attributes as $attr) {
                                        if ($attr->nodeName !== 'src' && $attr->nodeName !== 'poster'  && $attr->nodeName !== 'alt'  && $attr->nodeName !== 'class') {
                                            $options[$attr->nodeName] = $attr->nodeValue;
                                        }
                                    }
        /*
                                    $loaderOptions = [
                                        'style' => $imageFormat['options']??[]
                                    ];
                                    Html::addCssStyle($loaderOptions, [
                                        'quality' => 40,
                                        'blur' => 'true',
                                        'blur-radius' => 15,
                                        'blur-sigma' => 10,
                                    ]);
                                    $imageFormat['options'] = $loaderOptions['style'];
                                    $defaultImage = self::getUrl($file, false, [$name, $imageFormat['width'], $imageFormat['height'], $imageFormat['options']]);*/
                                    return File::getVideo($file, $name, $sizes, $responsive, $poster, $options, $wrapperOptions);
                                }
                            }
                        } else {
                            if (mb_stripos($src, 'youtube.com') !== false || mb_stripos($src, 'youtu.be') !== false || mb_stripos($src, 'youtube-nocookie.com') !== false) {
                                if (Yii::$app->getModule(Bootstrap::$id)->getPluginSettingValue('force_youtube_no_cookie')) {
                                    $srcParts = parse_url($src);
                                    $newQueryParts = [];
                                    $video = '';
                                    if (isset($srcParts['query'])) {
                                        $queryParts = explode('&', $srcParts['query']);
                                        foreach ($queryParts as $queryPart) {
                                            $queryPart = explode('=', $queryPart);
                                            if ($queryPart[0] === 'v') {
                                                $video = $queryPart[1] ?? '';
                                            } else {
                                                $newQueryParts[] = $queryPart[0] . '=' . ($queryPart[1] ?? '');
                                            }
                                        }
                                    }
                                    if ($video === '') {
                                        $path = ltrim($srcParts['path'], '/');
                                        $firstPath = mb_strpos($path, '/');
                                        if ($firstPath === false) {
                                            $firstPath = 0;
                                            $secondPath = null;
                                        } else {
                                            $firstPath++;
                                            $secondPath = mb_strpos($path, '/', $firstPath);
                                            if ($secondPath === false) {
                                                $secondPath = null;
                                            }
                                        }
                                        $video = mb_substr($path, $firstPath, $secondPath);
                                    }
                                    $src = 'https://www.youtube-nocookie.com/embed/' . $video . (count($newQueryParts) > 0 ? '?' . implode('&', $newQueryParts) : '');
                                }
                                $setup = [
                                    'techOrder' => ['youtube'],
                                    'sources' => [
                                        'type' => 'video/youtube',
                                        'src' => $src
                                    ],
                                    'fluid' => $responsive
                                ];
                                VideoJsYoutubeAsset::register(Yii::$app->view);
                                Html::addCssClass($wrapperOptions, 'fafcms-filemanager-video-wrapper');
                                return Html::tag('div', Html::tag('video', '', [
                                    'class' => 'video-js vjs-default-skin vjs-big-play-centered',
                                    'controls' => true,
                                    'data-setup' => $setup
                                ]), $wrapperOptions);
                            }
                            Html::addCssClass($wrapperOptions, 'fafcms-filemanager-image-wrapper');
                            return Html::tag('div', Html::img($src, $options), $wrapperOptions);
                        }
                    }
                    return '';
                },
            'allowedTypes' => [
                0,
            ],
        ];

        parent::init();
    }
}
