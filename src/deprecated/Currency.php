<?php

namespace fafcms\parser\deprecated;

use fafcms\parser\DeprecatedParserElement;
use Closure;
use Symfony\Component\DomCrawler\Crawler;
use Yii;

/**
 * Class Currency
 *
 * @package fafcms\parser\deprecated
 */
class Currency extends DeprecatedParserElement
{
    public $deprecatedName = 'currency';
    public $deprecatedReplacement;

    /**
     * {@inheritdoc}
     */
    public function init(): void
    {
        $this->deprecatedReplacement = [
            'replacement' => function($type, $parentTagName, $node, $crawler, $data, $language, $getRawData) {
                $currency = $node->hasAttribute('currency')?$node->getAttribute('currency'):null;
                if ($currency === null) {
                    $currencyChildren = $crawler->filterXPath($this->parser->name.'-'.$parentTagName.'/'.$this->parser->name.'-'.$parentTagName.'-currency');
                    if (count($currencyChildren) > 0) {
                        $currency = $this->parser->fullTrim($this->parser->parseElements($currencyChildren->html(), $this->parser->getName() . '-' . $parentTagName));
                    }
                }
                $value = $node->hasAttribute('value')?$node->getAttribute('value'):null;
                if ($value === null) {
                    $valueChildren = $crawler->filterXPath($this->parser->name.'-'.$parentTagName.'/'.$this->parser->name.'-'.$parentTagName.'-value');
                    if (count($valueChildren) > 0) {
                        $value = $this->parser->fullTrim($this->parser->parseElements($valueChildren->html(), $this->parser->getName() . '-' . $parentTagName));
                    }
                }
                if (empty($value)) {
                    $value = 0;
                }
                if (is_string($value) && is_numeric($value)) {
                    $value = (float) $value;
                }
                $locale = $node->hasAttribute('locale')?$node->getAttribute('locale'):null;
                if ($locale === null) {
                    $localeChildren = $crawler->filterXPath($this->parser->name.'-'.$parentTagName.'/'.$this->parser->name.'-'.$parentTagName.'-locale');
                    if (count($localeChildren) > 0) {
                        $locale = $this->parser->fullTrim($this->parser->parseElements($localeChildren->html(), $this->parser->getName() . '-' . $parentTagName));
                    }
                }
                if ($locale === null) {
                    $locale = Yii::$app->formatter->locale;
                }
                $forceSymbolPrefixed = $node->hasAttribute('force-symbol-prefixed')?$node->getAttribute('force-symbol-prefixed'):null;
                if ($forceSymbolPrefixed === null) {
                    $forceSymbolPrefixedChildren = $crawler->filterXPath($this->parser->name.'-'.$parentTagName.'/'.$this->parser->name.'-'.$parentTagName.'-force-symbol-prefixed');
                    if (count($forceSymbolPrefixedChildren) > 0) {
                        $forceSymbolPrefixed = $this->parser->fullTrim($this->parser->parseElements($forceSymbolPrefixedChildren->html(), $this->parser->getName() . '-' . $parentTagName));
                    }
                }
                $amountFractionDigits = $node->hasAttribute('amount-fraction-digits')?$node->getAttribute('amount-fraction-digits'):null;
                if ($amountFractionDigits === null) {
                    $amountFractionDigitsChildren = $crawler->filterXPath($this->parser->name.'-'.$parentTagName.'/'.$this->parser->name.'-'.$parentTagName.'-amount-fraction-digits');
                    if (count($amountFractionDigitsChildren) > 0) {
                        $amountFractionDigits = $this->parser->fullTrim($this->parser->parseElements($amountFractionDigitsChildren->html(), $this->parser->getName() . '-' . $parentTagName));
                    }
                }
                try {
                    $formatter = new NumberFormatter(strtolower($locale).'@currency='.$currency, NumberFormatter::CURRENCY);
                    $prefix = '';
                    if ($forceSymbolPrefixed == 'true') {
                        $pattern = $this->parser->fullTrim(str_replace('¤', '', $formatter->getPattern()));
                        $formatter->setPattern($pattern);
                        $prefix = $formatter->getSymbol(NumberFormatter::CURRENCY_SYMBOL).' ';
                    }
                    if($amountFractionDigits !== null && $amountFractionDigits >= 0){
                        //$formatter->setAttribute(NumberFormatter::MIN_FRACTION_DIGITS, 10);
                        //var_dump($formatter->getAttribute(NumberFormatter::FRACTION_DIGITS));
                        //die();
                        $pattern = substr($formatter->getPattern(),0, (-2 + $amountFractionDigits));
                        if ($amountFractionDigits == 0) {
                            $pattern .= '-';
                        }
                        $formatter->setPattern($pattern);
                    }
                    $parsedCurrency = $prefix.$formatter->formatCurrency($value, $currency);
                } catch (\Exception $e) {
                    Yii::$app->log->logger->log('Cannot format currency: '.$value.'. '.$e->getMessage(), Logger::LEVEL_ERROR);
                }
                return $parsedCurrency??'';
            },
        ];

        parent::init();
    }
}
