<?php

namespace fafcms\parser\deprecated;

use fafcms\parser\DeprecatedParserElement;
use Closure;
use Symfony\Component\DomCrawler\Crawler;
use Yii;

/**
 * Class Url
 *
 * @package fafcms\parser\deprecated
 */
class Url extends DeprecatedParserElement
{
    public $deprecatedName = 'url';
    public $deprecatedReplacement;

    /**
     * {@inheritdoc}
     */
    public function init(): void
    {
        $this->deprecatedReplacement = [
            'replacement' => function($type, $parentTagName, $node, $crawler, $data, $language) {
                $scheme = false;
                if ($node->hasAttribute('scheme')) {
                    $scheme = $node->getAttribute('scheme');
                    if ($scheme === 'true') {
                        $scheme = true;
                    } elseif ($scheme === 'false') {
                        $scheme = false;
                    }
                }
                $url = $this->parser->fullTrim($this->parser->parseElements($crawler->html(), $this->parser->getName() . '-' . $parentTagName));
                return \yii\helpers\Url::to($url, $scheme);
            },
        ];

        parent::init();
    }
}
