<?php

namespace fafcms\parser\deprecated;

use fafcms\parser\DeprecatedParserElement;
use Closure;
use fafcms\settingmanager\Bootstrap as SettingmanagerBootstrap;
use fafcms\sitemanager\models\Contentmeta;
use fafcms\sitemanager\models\ContentmetaTopic;
use Symfony\Component\DomCrawler\Crawler;
use Yii;
use fafcms\sitemanager\models\Snippet;

/**
 * Class Contentmetas
 *
 * @package fafcms\parser\deprecated
 */
class Contentmetas extends DeprecatedParserElement
{
    public $deprecatedName = 'contentmetas';
    public $deprecatedReplacement;

    /**
     * {@inheritdoc}
     */
    public function init(): void
    {
        $this->deprecatedReplacement = [
            'replacement' => function($type, $parentTagName, $node, $crawler, $data, $language) {
                $snippetId = $node->hasAttribute('snippet')?$node->getAttribute('snippet'):Yii::$app->getModule(SettingmanagerBootstrap::$id)->getSetting('contentmeta_default_snippet', null);
                $limit = $node->hasAttribute('limit')?$node->getAttribute('limit'):Yii::$app->getModule(SettingmanagerBootstrap::$id)->getSetting('contentmeta_default_limit', 9999); //todo
                $topic = $node->hasAttribute('topic')?$node->getAttribute('topic'):null;
                $contentsOnly = $node->hasAttribute('contents-only')?$node->getAttribute('contents-only'):true;

                if ($topic === null) {
                    $topicChildren = $crawler->filterXPath($this->parser->name.'-'.$parentTagName.'/'.$this->parser->name.'-'.$parentTagName.'-topic');
                    if (count($topicChildren) > 0) {
                        $topic = $this->parser->fullTrim($this->parser->parseElements($topicChildren->html(), $this->parser->getName() . '-' . $parentTagName));
                    }
                }

                if ($snippetId === null) {
                    return '';
                }

                $snippet = Snippet::getContentById($snippetId);

                if ($snippet === null) {
                    return '';
                }

                $contentmeta = Contentmeta::find();

                if ($topic !== null) {
                    $contentmeta = $contentmeta->innerJoinWith('contentmetaTopics')->where([
                        ContentmetaTopic::tableName().'.topic_id' => $topic
                    ]);
                }

                $contentmeta = $contentmeta->limit($limit);

                if ($contentsOnly) {
                    $data['contentmetas'] = $contentmeta->contents();
                } else {
                    $data['contentmetas'] = $contentmeta->all();
                }

                return $this->parser->parse($snippet, $parentTagName);
            },
            'allowedTypes' => [
                0,
            ],
        ];

        parent::init();
    }
}
