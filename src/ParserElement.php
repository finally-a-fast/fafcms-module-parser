<?php

namespace fafcms\parser;

use fafcms\parser\component\Parser;
use yii\base\BaseObject;

/**
 * Class ParserElement
 * @package fafcms\parser
 */
abstract class ParserElement extends BaseObject
{
    /**
     * @var Parser
     */
    private $_parser;

    /**
     * @return Parser
     */
    public function getParser(): ?Parser
    {
        return $this->_parser;
    }

    /**
     * @param Parser $parser
     */
    public function setParser(Parser $parser): void
    {
        $this->_parser = $parser;
    }

    public $prefixParserName = true;

    /**
     * @var bool specify if content should be parsed. If set to false content will return raw data.
     */
    public bool $parseContent = true;

    /**
     * @var array Parsed data of element
     */
    public $data = [];

    /**
     * @var array Raw attributes of element
     */
    public $attributes = [];

    /**
     * @var array Raw child elements of element
     */
    public $elements = [];

    /**
     * @var Content of element
     */
    public $content;

    /**
     * @return ElementSetting[]|null
     */
    public function elementSettings(): ?array
    {
        return null;
    }

    /**
     * @return string
     */
    abstract public function name(): string;

    /**
     * @return string
     */
    abstract public function description(): string;

    /**
     * @return array
     */
    public function editorOptions(): array
    {
        return ['tag' => 'div'];
    }

    /**
     * @return array|null
     */
    public function allowedTypes(): ?array
    {
        return null;
    }

    /**
     * @return array|null
     */
    public function allowedParents(): ?array
    {
        return null;
    }

    /**
     * Initializes parser element and executes bootstrap components.
     * This method is called by parser component after loading available parser elements.
     * If you override this method, make sure you also call the parent implementation.
     */
    public function bootstrap(): void
    {

    }

    /**
     * @return string
     */
    public function tagName(): string
    {
        return ($this->prefixParserName ? $this->parser->name . '-' : '') . $this->name();
    }

    /**
     * @return mixed
     */
    abstract public function run();
}
