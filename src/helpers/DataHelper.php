<?php

namespace fafcms\parser\helpers;

use fafcms\parser\component\Parser;
use Yii;
use yii\base\BaseObject;
use yii\base\InvalidArgumentException;

/**
 * Class DataHelper
 *
 * @package fafcms\parser\helpers
 */
class DataHelper extends BaseObject
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var mixed
     */
    public $value;

    /**
     * @param string      $name
     * @param             $value
     * @param Parser|null $parser
     */
    private static function setData(string $name, $value, ?Parser $parser = null): void
    {
        if ($parser === null) {
            $parser = Yii::$app->fafcmsParser;
        }

        $parser->setAttributeData($name, $value);
    }

    /**
     * @param array       $rawParams
     * @param false       $setAttributeData
     * @param Parser|null $parser
     *
     * @return array
     */
    public static function formatParams(array $rawParams, $setAttributeData = false, ?Parser $parser = null): array
    {
        $params = [];

        foreach ($rawParams as $name => $value) {
            if ($value instanceof self) {
                $name  = $value->name;
                $value = $value->value;
            }

            if ($name === null) {
                if ($setAttributeData) {
                    throw new InvalidArgumentException('To set data the name attribute is required.');
                }

                $params[] = self::formatValue($value, $parser);
            } else {
                $params[$name] = self::formatValue($value, $parser);

                if ($setAttributeData) {
                    self::setData($name, $params[$name]);
                }
            }
        }

        return $params;
    }

    /**
     * @param             $value
     * @param Parser|null $parser
     *
     * @return bool|float|int|mixed|string
     */
    public static function formatValue($value, ?Parser $parser = null)
    {
        if (is_array($value) || is_object($value)) {
            return $value;
        }

        if ($parser === null) {
            $parser = Yii::$app->fafcmsParser;
        }

        if (is_numeric($value)) {
            if (strpos($value, '.') === false) {
                $value = (int)$value;
            } else {
                $value = (float)$value;
            }
        } elseif ($value === 'true' || $value === 't') {
            $value = true;
        } elseif ($value === 'false' || $value === 'f') {
            $value = false;
        } elseif (strpos($value, '\'') === 0 || strpos($value, '"') === 0) {
            $value = stripslashes(substr($value, 1, -1));
        } else {
            $value = $parser->getAttributeData($value);
        }

        return $value;
    }
}
