<?php

namespace fafcms\parser;

use yii\base\Application;
use fafcms\helpers\abstractions\PluginBootstrap;
use fafcms\helpers\abstractions\PluginModule;
use yii\base\BootstrapInterface;
use yii\i18n\PhpMessageSource;

class Bootstrap extends PluginBootstrap implements BootstrapInterface
{
    public static $id = 'fafcms-parser';

    protected function bootstrapTranslations(Application $app, PluginModule $module): bool
    {
        if (!isset($app->i18n->translations['fafcms-parser'])) {
            $app->i18n->translations['fafcms-parser'] = [
                'class' => PhpMessageSource::class,
                'basePath' => __DIR__ .'/messages',
                'forceTranslation' => true,
            ];
        }

        return true;
    }

    protected function bootstrapApp(Application $app, PluginModule $module): bool
    {
        return true;
    }

    protected function bootstrapWebApp(Application $app, PluginModule $module): bool
    {
        return true;
    }

    protected function bootstrapConsoleApp(Application $app, PluginModule $module): bool
    {
        return true;
    }

    protected function bootstrapBackendApp(Application $app, PluginModule $module): bool
    {
        return true;
    }

    protected function bootstrapFrontendApp(Application $app, PluginModule $module): bool
    {
        return true;
    }
}
