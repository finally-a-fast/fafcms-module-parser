<?php

namespace fafcms\parser\elements;

use Faf\TemplateEngine\Parser;
use Faf\TemplateEngine\Helpers\ParserElement;
use Yii;

/**
 * Class EndBody
 *
 * @package fafcms\parser\elements
 */
class EndBody extends ParserElement
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'end-body';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-parser', 'End body');
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        ob_start();
        PHP_VERSION_ID >= 80000 ? ob_implicit_flush(false) : ob_implicit_flush(0);
        Yii::$app->view->endBody();

        return ob_get_clean();
    }

    public function allowedTypes(): ?array
    {
        return [
            Parser::TYPE_HTML
        ];
    }
}
