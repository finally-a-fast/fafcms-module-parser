<?php


namespace fafcms\parser\elements;

use Yii;
use Faf\TemplateEngine\Helpers\ElementSetting;
use Faf\TemplateEngine\Helpers\ParserElement;

/**
 * Class Select
 *
 * @package fafcms\parser\elements
 */
class Select extends ParserElement
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'query-select';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-parser', 'Select');
    }

    public function allowedParents(): ?array
    {
        return [Query::class];
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return $this->getParser()->fullTrim($this->content);
    }
}
