<?php

namespace fafcms\parser\elements;

use Yii;
use Faf\TemplateEngine\Helpers\ParserElement;

/**
 * Class Order
 *
 * @package fafcms\parser\elements
 */
class Order extends ParserElement
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'query-order';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-parser', 'Order');
    }

    public function allowedParents(): ?array
    {
        return [Query::class];
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return $this->getParser()->fullTrim($this->content);
    }
}
