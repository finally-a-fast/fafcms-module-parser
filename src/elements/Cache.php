<?php

namespace fafcms\parser\elements;

use Faf\TemplateEngine\Helpers\ElementSetting;
use Faf\TemplateEngine\Helpers\ParserElement;
use Yii;
use Yiisoft\Validator\Rule\Required;

/**
 * Class Cache
 *
 * @package fafcms\parser\elements
 */
class Cache extends ParserElement
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'cache';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-parser', 'Cache');
    }

    /**
     * {@inheritdoc}
     */
    public function elementSettings(): array
    {
        return [
            new ElementSetting([
               'name'      => 'key',
               'aliases'   => ['index'],
               'label'     => Yii::t('fafcms-parser', 'Key'),
            ]),
            new ElementSetting([
               'name'      => 'query',
               'aliases'   => ['q'],
               'label'     => Yii::t('fafcms-parser', 'Query'),
               'element'   => Query::class,
               'rules'     => [
                   (new Required())->when(function () {
                       return empty($this->data['key']);
                   })
               ],
            ]),
            new ElementSetting([
               'name'         => 'duration',
               'aliases'      => ['time'],
               'label'        => Yii::t('fafcms-parser', 'Duration'),
               'defaultValue' => 86400,
               'rules'        => [
                   new Required()
               ],
           ]),
            new ElementSetting([
               'name'      => 'content',
               'aliases'   => ['value'],
               'label'     => Yii::t('fafcms-parser', 'Content'),
               'element'   => CacheContent::class,
               'rawData'   => true
           ]),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        if ($this->data['query']) {
            $key = $this->data['query'];
        }

        if ($this->data['key']) {
            $key = $this->data['key'] . (isset($key) ? '_' . $key : '');
        }

        if (empty($this->data['content'])) {
            return '';
        }

        return Yii::$app->getCache()->getOrSet(Yii::$app->fafcms->getCacheName($key), function () {
            return $this->getParser()->fullTrim($this->data['content']);
        }, $this->data['duration']);
    }
}
