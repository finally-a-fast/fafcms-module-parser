<?php

namespace fafcms\parser\elements;

use Faf\TemplateEngine\Helpers\ElementSetting;
use Faf\TemplateEngine\Helpers\ParserElement;
use Yii;
use Yiisoft\Validator\Rule\Required;

/**
 * Class Translation
 *
 * @package fafcms\parser\elements
 */
class Translation extends ParserElement
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'translation';
    }

    public function aliases(): array
    {
        return ['translate', 't'];
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-parser', 'Translation');
    }

    /**
     * {@inheritdoc}
     */
    public function elementSettings(): array
    {
        return [
            new ElementSetting([
                'name'      => 'category',
                'label'     => Yii::t('fafcms-parser', 'Category'),
               //Todo
               //'element'   => TranslationCategory::class,
                'defaultValue' => 'app',
                'rules'     => [
                    new Required()
                ],
            ]),
            new ElementSetting([
                'name'      => 'message',
                'label'     => Yii::t('fafcms-parser', 'Message'),
                //Todo
                //'element'   => TranslationMessage::class,
                'content'   => true,
            ]),
            new ElementSetting([
                'name'         => 'params',
                'label'        => Yii::t('fafcms-parser', 'Params'),
               //Todo
               //'element'     => TranslationCategory::class,
                'rawData'      => true,
                'defaultValue' => [],
            ]),
            new ElementSetting([
                'name'         => 'language',
                'label'        => Yii::t('fafcms-parser', 'Language'),
               //Todo
               //'element'     => TranslationCategory::class,
                'defaultValue' => null,
            ]),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return Yii::t($this->data['category'], $this->data['message'], $this->data['params'], $this->data['language']);
    }
}
