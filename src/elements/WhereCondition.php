<?php

namespace fafcms\parser\elements;

use Yii;
use Faf\TemplateEngine\Helpers\ElementSetting;
use Faf\TemplateEngine\Helpers\ParserElement;
use Yiisoft\Validator\Rule\Required;

/**
 * Class Where
 *
 * @package fafcms\parser\elements
 */
class WhereCondition extends ParserElement
{
    public bool $contentAsRawData = true;

    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'query-where-condition';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-parser', 'Where');
    }

    /**
     * {@inheritdoc}
     */
    public function elementSettings(): array
    {
        return [
            new ElementSetting([
                'name'      => 'attr',
                'aliases'   => ['attribute', 'column'],
                'label'     => Yii::t('fafcms-parser', 'Attribute'),
                'rules'     => [
                   new Required()
                ],
            ]),
            new ElementSetting([
                'name'      => 'type',
                'label'     => Yii::t('fafcms-parser', 'Type'),
                'rules'     => [
                    new Required()
                ],
            ]),
            new ElementSetting([
               'name'      => 'value',
               'label'     => Yii::t('fafcms-parser', 'Value'),
               'content'   => true,
               'rawData'   => true,
            ]),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function allowedParents(): ?array
    {
        return [Where::class, WhereAnd::class, WhereOr::class];
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return [
            $this->data['type'], $this->data['attr'], $this->data['value'],
        ];
    }
}
