<?php


namespace fafcms\parser\elements;


use Faf\TemplateEngine\Helpers\ElementSetting;
use Faf\TemplateEngine\Helpers\ParserElement;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper as YiiArrayHelper;
use Yiisoft\Validator\DataSetInterface;
use Yiisoft\Validator\Rule\InRange;
use Yiisoft\Validator\Rule\Required;

/**
 * Class ArrayHelper
 *
 * @package fafcms\parser\elements
 */
class ArrayHelper extends ParserElement
{
    /**
     * {@inheritdoc }
     */
    public bool $parseContent = false;

    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'array-helper';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-parser', 'Array Helper');
    }

    /**
     * {@inheritdoc}
     */
    public function elementSettings(): array
    {
        $a = $this;

        return [
            new ElementSetting([
               'name'      => 'function',
               'aliases'   => ['method'],
               'label'     => Yii::t('fafcms-parser', 'Function'),
               'rules'     => [
                   new InRange(['column', 'map']),
                   new Required()
               ],
            ]),
            new ElementSetting([
                'name'      => 'column',
                'aliases'   => ['key'],
                'label'     => Yii::t('fafcms-parser', 'Key'),
                'rules'     => [
                    (new Required())->when(function ($value, $d) {
                        return $this->data['function'] === 'column';
                    })
                ],
            ]),
            new ElementSetting([
                'name'      => 'from',
                'aliases'   => ['key'],
                'label'     => Yii::t('fafcms-parser', 'From'),
                'rules'     => [
                    (new Required())->when(function ($value) {
                        return $this->data['function'] === 'map';
                    })
                ],
            ]),
            new ElementSetting([
                'name'      => 'to',
                'aliases'   => ['key'],
                'label'     => Yii::t('fafcms-parser', 'To'),
                'rules'     => [
                    (new Required())->when(function ($value) {
                        return $this->data['function'] === 'map';
                    })
                ],
            ]),
            new ElementSetting([
               'name'      => 'array',
               'aliases'   => ['content'],
               'label'     => Yii::t('fafcms-parser', 'Function'),
               'element'   => ArrayHelperArray::class,
               'rawData'   => true,
           ]),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $data = $this->data['array'];

        switch ($this->data['function']) {
            case 'column':
                if ($data instanceof ActiveRecord) {
                    $data = $data->getAttributes();
                } elseif (isset($data[0]) && $data[0] instanceof ActiveRecord) {
                    /** @var ActiveRecord $value */
                    foreach ($data as $index => $value) {
                        $data[$index] = $value->getAttributes();
                    }
                }

                return YiiArrayHelper::getColumn($data, $this->data['column']);
            case 'map':
                return YiiArrayHelper::map($data, $this->data['from'], $this->data['to']);
            default:
                return [];
        }
    }
}
