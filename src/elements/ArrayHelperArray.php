<?php


namespace fafcms\parser\elements;


use Faf\TemplateEngine\Helpers\ParserElement;
use Yii;

class ArrayHelperArray extends ParserElement
{
    /**
     * {@inheritdoc }
     */
    public bool $contentAsRawData = true;

    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'array-helper-array';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-parser', 'Array Helper array');
    }

    /**
     * {@inheritdoc}
     */
    public function allowedParents(): ?array
    {
        return [ArrayHelper::class];
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return $this->content;
    }
}
