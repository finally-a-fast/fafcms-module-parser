<?php

namespace fafcms\parser\elements;

use Faf\TemplateEngine\Parser;
use Faf\TemplateEngine\Helpers\ParserElement;
use Yii;

/**
 * Class Head
 *
 * @package fafcms\parser\elements
 */
class Head extends ParserElement
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'head';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-parser', 'Head');
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        ob_start();
        PHP_VERSION_ID >= 80000 ? ob_implicit_flush(false) : ob_implicit_flush(0);
        Yii::$app->view->head();

        return '<title>' . Yii::$app->view->title . '</title>' . ob_get_clean();
    }

    public function allowedTypes(): ?array
    {
        return [
            Parser::TYPE_HTML
        ];
    }
}
