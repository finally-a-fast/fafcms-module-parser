<?php

namespace fafcms\parser\elements;

use Faf\TemplateEngine\Helpers\ElementSetting;
use Faf\TemplateEngine\Helpers\ParserElement;
use Yii;
use Yiisoft\Validator\Rule\Required;

/**
 * Class Placeholder
 *
 * @package fafcms\parser\elements
 * @deprecated Please use \Faf\TemplateEngine\Elements\Get instead
 */
class Placeholder extends ParserElement
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'placeholder';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-parser', 'Placeholder');
    }

    /**
     * {@inheritdoc}
     */
    public function elementSettings(): array
    {
        return [
            new ElementSetting([
               'name'     => 'attribute',
               'label'    => Yii::t('fafcms-parser', 'Attribute'),
               'safeData' => false,
               'content'  => true
           ]),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $attributeData = $this->parser->getAttributeData($this->data['attribute'], $data);

        if (is_array($attributeData) || is_object($attributeData)) {
            return json_encode($attributeData);
        }

        $this->parser->logger->warning('Usage of "' . $this->tagName() . '" is deprecated. Please use "' . $this->parser->name . '-get" instead.');

        return $attributeData;
    }
}
