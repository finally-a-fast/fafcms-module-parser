<?php

namespace fafcms\parser\elements;

use Yii;
use Faf\TemplateEngine\Helpers\ElementSetting;
use Faf\TemplateEngine\Helpers\ParserElement;

/**
 * Class Where
 *
 * @package fafcms\parser\elements
 */
class Where extends ParserElement
{
    public bool $contentAsRawData = true;

    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'query-where';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-parser', 'Where');
    }

    /**
     * {@inheritdoc}
     */
    public function allowedParents(): ?array
    {
        return [Query::class];
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $data = $this->content;

        if (isset($data[0]) && is_array($data[0])) {
            $data = array_merge(['AND'], $data);
        }

        return $data;
    }
}
