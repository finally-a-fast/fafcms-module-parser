<?php

namespace fafcms\parser\elements;

use Faf\TemplateEngine\Parser;
use Faf\TemplateEngine\Helpers\ParserElement;
use Yii;

/**
 * Class BeginBody
 *
 * @package fafcms\parser\elements
 */
class BeginBody extends ParserElement
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'begin-body';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-parser', 'Begin body');
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        ob_start();
        PHP_VERSION_ID >= 80000 ? ob_implicit_flush(false) : ob_implicit_flush(0);
        Yii::$app->view->beginBody();

        return ob_get_clean();
    }

    public function allowedTypes(): ?array
    {
        return [
            Parser::TYPE_HTML
        ];
    }
}
