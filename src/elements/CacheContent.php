<?php


namespace fafcms\parser\elements;


use Faf\TemplateEngine\Helpers\ParserElement;
use Yii;

/**
 * Class CacheContent
 *
 * @package fafcms\parser\elements
 */
class CacheContent extends ParserElement
{
    public bool $parseContent = false;

    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'cache-content';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-parser', 'Cache Content');
    }

    /**
     * {@inheritdoc}
     */
    public function allowedParents(): ?array
    {
        return [Cache::class];
    }

    /**
     * {@inheritDoc}
     */
    public function run()
    {
        return $this->getParser()->parseElements($this->content, $this->tagName(), true);
    }
}
