<?php

namespace fafcms\parser\elements;

use Yii;
use fafcms\helpers\ActiveRecord;
use Faf\TemplateEngine\Helpers\ElementSetting;
use Faf\TemplateEngine\Helpers\ParserElement;
use yii\base\ErrorException;
use Yiisoft\Validator\Rule\Boolean;
use Yiisoft\Validator\Rule\Required;

/**
 * Class Query
 *
 * @package fafcms\parser\elements
 */
class Query extends ParserElement
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'query';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-parser', 'Query');
    }

    /**
     * {@inheritdoc}
     */
    public function elementSettings(): array
    {
        return [
            new ElementSetting([
               'name'      => 'model',
               'aliases'   => ['class'],
               'label'     => Yii::t('fafcms-parser', 'Model'),
               'rules'     => [
                   new Required()
               ],
           ]),
            new ElementSetting([
               'name'      => 'condition',
               'label'     => Yii::t('fafcms-parser', 'Condition'),
               'element'   => Where::class,
               'rawData'   => true,
               'multiple'  => false,
               'rules'     => [
                   new Required()
               ],
           ]),
            new ElementSetting([
               'name'         => 'select',
               'label'        => Yii::t('fafcms-parser', 'Select'),
               'element'      => Select::class,
               'defaultValue' => '*',
               'rules'        => [
                   new Required()
               ],
           ]),
            new ElementSetting([
               'name'         => 'format',
               'aliases'      => ['call', 'action', 'type'],
               'label'        => Yii::t('fafcms-parser', 'Format'),
               'defaultValue' => 'all',
               'rules'        => [
                   new Required()
               ],
           ]),
            new ElementSetting([
                'name'      => 'array',
                'aliases'   => ['asArray'],
                'label'     => Yii::t('fafcms-parser', 'As Array'),
                'defaultValue' => false,
                'rules'     => [
                    new Boolean()
                ],
            ]),
            new ElementSetting([
               'name'      => 'order',
               'aliases'   => ['orderBy'],
               'label'     => Yii::t('fafcms-parser', 'Order'),
               'element'   => Order::class,
           ]),
        ];
    }

    /**
     * {@inheritdoc}
     * @throws ErrorException
     */
    public function run()
    {
        /** @var ActiveRecord $modelClass */
        $modelClass = $this->data['model'];
        $query      = $modelClass::find()->select($this->data['select'])->where($this->data['condition']);

        if ($this->data['order'] !== null) {
            $query = $query->orderBy($this->data['order']);
        }

        if ($this->data['format'] === 'raw') {
            return $query->createCommand()->getRawSql();
        }

        if ($this->data['array'] === true) {
            $query = $query->asArray();
        }

        if ($query->hasMethod($this->data['format'])) {
            return $query->{$this->data['format']}();
        }

        throw new ErrorException(Yii::t('fafcms-parser', 'Undefined Format: ' . $this->data['format']));
    }
}
