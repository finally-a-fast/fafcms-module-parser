<?php

namespace fafcms\parser\elements;

use Faf\TemplateEngine\Helpers\ElementSetting;
use Faf\TemplateEngine\Helpers\ParserElement;
use fafcms\sitemanager\models\Snippet as SnippetModel;
use Yii;
use Yiisoft\Validator\Rule\Required;

/**
 * Class Snippet
 *
 * @package fafcms\parser\elements
 */
class Snippet extends ParserElement
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'snippet';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-parser', 'Snippet');
    }

    /**
     * {@inheritdoc}
     */
    public function elementSettings(): array
    {
        return [
            new ElementSetting([
               'name'      => 'id',
               'label'     => Yii::t('fafcms-parser', 'Id'),
               //Todo
               //'element'   => SnippetId::class,
               'rules'     => [
                    new Required()
                ],
           ]),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $snippet = SnippetModel::getContentById($this->data['id']);

        if ($snippet === null) {
            return '';
        }

        return $this->parser->parseElements($snippet, $this->tagName());
    }
}
