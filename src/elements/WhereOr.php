<?php

namespace fafcms\parser\elements;

use Yii;
use Faf\TemplateEngine\Helpers\ElementSetting;
use Faf\TemplateEngine\Helpers\ParserElement;
use Yiisoft\Validator\Rule\Required;

class WhereOr extends ParserElement
{
    public bool $contentAsRawData = true;

    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'query-where-or';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-parser', 'Where');
    }

    /**
     * {@inheritdoc}
     */
    public function elementSettings(): array
    {
        return [
            new ElementSetting([
               'name'      => 'condition',
               'label'     => Yii::t('fafcms-parser', 'Condition'),
               'element'   => WhereCondition::class,
               'rawData'   => true,
               'content'   => true,
               'multiple'  => false,
               'rules'     => [
                   new Required()
               ],
            ]),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function allowedParents(): ?array
    {
        return [Where::class];
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return [
            'OR', $this->data['condition'],
        ];
    }
}
