[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Changelog | Module parser
================================================

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Changed
- Replaced parser with the new fafte parser @cmoeke
- Rewritten some parser elements @cmoeke

## [0.1.0] - 2020-09-29
### Added
- CHANGELOG.md @cmoeke fafcms-core#39
- Basic doc folder structure @cmoeke fafcms-core#37
- .gitlab-ci.yml for doc an build creation @cmoeke fafcms-core#38
- Required php module to composer.json @cmoeke
- Possibility to use closures as child array of parser data @cmoeke
- Added allowedChildCache to improve performance @cmoeke
- Added option to use attribute name as key by usage of multiple element settings @cmoeke
- Added get element @cmoeke
- Added set element @cmoeke
- Added possibility in getAttributeData to disable automatic execution of functions if the last element of attribute data is a function @cmoeke
- Added parser element to call functions @cmoeke
- Added general param parser elements @cmoeke
- Added clean parser data handler @cmoeke
- Added parser element to cache data @StefanBrandenburger
- Added parser elements to build ActiveRecord queries @StefanBrandenburger
- Added asArray element setting to query element @StefanBrandenburger
- Added array-helper elements @StefanBrandenburger
- Added order child element to query element @cmoeke

### Changed
- composer.json to use correct dependencies @cmoeke
- LICENSE to LICENSE.md @cmoeke
- Profiling is now only active when the debug mode is active @cmoeke
- Changed get and set elements to use general param element @cmoeke
- Elements now dont always return an array while parsing raw data @StefanBrandenburger
- Param element now parses the content of param-value @StefanBrandenburger
- PHP dependency to 7.4 @cmoeke
- Changed ci config to handle new build branch @cmoeke

### Fixed
- Broken README.md icons @cmoeke fafcms-core#46
- Fixed multiple usage of element settings as child elements @cmoeke
- Fixed allowedChildCache for different parser types @cmoeke
- Fixed space in param-name element while using in set element @StefanBrandenburger
- Fixed formatValue in DataHelper when value is an array @StefanBrandenburger

[Unreleased]: https://gitlab.com/finally-a-fast/fafcms-module-parser/-/tree/master
[0.1.0]: https://gitlab.com/finally-a-fast/fafcms-module-parser/-/tree/v0.1.0-alpha
